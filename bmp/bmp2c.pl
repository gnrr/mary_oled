#!/usr/bin/perl -w

#########################################################################
# bmp2c
#========================================================================
# Convert BMP File to C source.
#------------------------------------------------------------------------
# Rev.01 2010.10.24 Munetomo Maruyama  1st Release
#########################################################################

use Getopt::Long;

#======================================
#--------------------------------------
# Main Routine
#--------------------------------------
#======================================
#-----------------------------
# Check Command Line Arguments
#-----------------------------
####if (($#ARGV != 0) && ($#ARGV != 2) && ($#ARGV != 1) && ($#ARGV != 3))
#### $#ARGV != 0 : inputBMP.bmp 
#### $#ARGV != 2 : inputBMP.bmp    -o outputRGB.rgb  
#### $#ARGV != 1 : inputBMP.bmp -a
#### $#ARGV != 3 : inputBMP.bmp -a -o outputStatic.rgb
sub error_msg();
sub error_msg()
{
    printf("-----------------------------------------------------------------------------------\n"); 
    printf("[Usage] Convert BMP File to C source.\n"); 
    printf("bmp2c input.bmp\ [-o hoge.h] [-p hoge.h] [-x posx] [-y posy] [-w width] [-h height]\n"); 
    printf("-----------------------------------------------------------------------------------\n"); 
    printf("bmp2c input.bmp  ... Generate input.bmp.rgb.h as Graphic Data Array.\n");
    printf("-- If input file is BMP32, output data length will be 32bit.\n");
    printf("-- If input file is BMP24, output data length will be 32bit.\n");
    printf("-- If input file is BMP8,  output data length will be 8bit,\n");
    printf("   And input.bmp.plt.h also be generated as its Color Palette if the BMP has.\n");
    printf("-- You can specify output file name of Graphic Data Array by -o option.\n");
    printf("-- You can specify output file name of Color Palette by -p option.\n");
    printf("   The -p option will be ignored if input bmp file does not have Color Palette.\n");
    printf("-- You can clip the source bmp by -x, -y, -w, -h options.\n");
    printf("---------------------------------------------------------------------------------------\n"); 
    die "\n"; 
}

#---------------------------
# Parse Command Options
#---------------------------
if ($#ARGV == -1)
{
    &error_msg();
}
$opt_o = '';
$opt_p = '';
$opt_x = -1;
$opt_y = -1;
$opt_w = -1;
$opt_h = -1;
$result_option = GetOptions('output=s'   => \$opt_o,  ## -o or --output  with mandatory argument
                            'palette=s'  => \$opt_p,  ## -p or --palette with mandatory argument
                            'xpos=i'     => \$opt_x,  ## -x or --xpos    with mandatory argument
                            'ypos=i'     => \$opt_y,  ## -y or --ypos    with mandatory argument
                            'width=i'    => \$opt_w,  ## -w or --width   with mandatory argument
                            'height=i'   => \$opt_h); ## -h or --height  with mandatory argument
if ($result_option == 0)
{
    &error_msg();
}
#---------------------------
# Check Option Parameters
#---------------------------
$fname_bmp = $ARGV[$#ARGV];
if ($opt_o eq '')
{
    $fname_rgb = $fname_bmp.".rgb.h";
}
else
{
    $fname_rgb = $opt_o;
}

if ($opt_p eq '')
{
    $fname_plt = $fname_bmp.".plt.h";
}
else
{
    $fname_plt = $opt_p;
}

if ($fname_rgb eq $fname_plt)
{
    &error_msg();
}

#----------------
# Get Base Name
#----------------
$basename = $fname_bmp;
$basename =~ s/\./_/g;

#---------------------------
# Open Files
#---------------------------
if (! -f $fname_bmp)
{
  die "$fname_bmp does not exist. \n";
}
open (IN_bmp , "< $fname_bmp");
binmode(IN_bmp);

#--------------------------
# Read BMP Header and Copy
#--------------------------
$ptrbmp = 0;

read (IN_bmp, $buf, 2); #BM         ## 2
$ptrbmp = $ptrbmp + 2;
#
read (IN_bmp, $buf, 4); #fileSize   ## 6
$ptrbmp = $ptrbmp + 4;
$fileSize = unpack("L1", $buf);
#
read (IN_bmp, $buf, 4); #zero       ## 10
$ptrbmp = $ptrbmp + 4;
#
read (IN_bmp, $buf, 4); #bodyOffset ## 14
$ptrbmp = $ptrbmp + 4;
$bodyOffset = unpack("L1", $buf);
#
read (IN_bmp, $buf, 4); #infosize   ## 18
$ptrbmp = $ptrbmp + 4;
$infoSize = unpack("L1", $buf);
#
read (IN_bmp, $buf, 4); #width      ## 22
$ptrbmp = $ptrbmp + 4;
$width = unpack("L1", $buf);
#
read (IN_bmp, $buf, 4); #height     ## 26
$ptrbmp = $ptrbmp + 4;
$height = unpack("L1", $buf);
#
read (IN_bmp, $buf, 2); #planeCount ## 28
$ptrbmp = $ptrbmp + 2;
$planeCount = unpack("S1", $buf);
#
read (IN_bmp, $buf, 2); #pixelDepth ## 30
$ptrbmp = $ptrbmp + 2;
$pixelDepth = unpack("S1", $buf);
if (($pixelDepth != 8) && ($pixelDepth != 24) && ($pixelDepth != 32))
{
    die ("Unsupported Format: pixelDepth=%d\n", $pixelDepth);
}
#
read (IN_bmp, $buf, 4); #zero       ## 34
$ptrbmp = $ptrbmp + 4;
$compression = unpack("L1", $buf);
if ($compression != 0)
{
    die ("Unsupported Format: compression=%d\n", $compression);
}
#
read (IN_bmp, $buf, 4); #bodySize   ## 38
$ptrbmp = $ptrbmp + 4;
$bodySize = unpack("L1", $buf);
#
read (IN_bmp, $buf, 4); #skip       ## 42
$ptrbmp = $ptrbmp + 4;
#
read (IN_bmp, $buf, 4); #skip       ## 46
$ptrbmp = $ptrbmp + 4;
#
read (IN_bmp, $buf, 4); #cpUsed     ## 50
$ptrbmp = $ptrbmp + 4;
$cpUsed = unpack("L1", $buf);
if (($cpUsed == 0) && ($pixelDepth == 8))
{
    $cpUsed = 256;
}
#
read (IN_bmp, $buf, 4); #cpPrimary  ## 54(0x36)
$ptrbmp = $ptrbmp + 4;
$cpPrimary = unpack("L1", $buf);

printf("fileSize   = %d\n"  , $fileSize);
printf("bodyOffset = 0x%x\n", $bodyOffset);
printf("infoSize   = %d\n"  , $infoSize);
printf("width      = %d\n"  , $width);
printf("height     = %d\n"  , $height);
printf("planeCount = %d\n"  , $planeCount);
printf("pixelDepth = %d\n"  , $pixelDepth);
printf("bodySize   = %d\n"  , $bodySize);
printf("colorPalletUsed       = %d\n", $cpUsed);
printf("colorPalletImportant  = %d\n", $cpPrimary);

#-------------------
# Check Clip Region
#-------------------
if ($opt_x >= 0)
{
    if ($opt_x > $width)
    {
        &error_msg();
    }
    $x0 = $opt_x;
}
else
{
    $x0 = 0;
}

if ($opt_y >= 0)
{
    if ($opt_y > $height)
    {
        &error_msg();
    }
    $y0 = $opt_y;
}
else
{
    $y0 = 0;
}

if ($opt_w >= 0)
{
    $x1 = $x0 + $opt_w;
    
    if ($x1 > $width)
    {
        &error_msg();
    }
}
else
{
    $x1 = $width;
}

if ($opt_h >= 0)
{
    $y1 = $y0 + $opt_h;
    if ($y1 > $height)
    {
        &error_msg();
    }
}
else
{
    $y1 = $height;
}

#---------------------------
# Read Color Pallet, if have
#---------------------------
if ($cpUsed != 0)
{
    ####printf("ColorPallet ptrbmp = 0x%0x\n", $ptrbmp);
    for ($i = 0; $i < $cpUsed; $i++)
    {
        read (IN_bmp, $buf, 4); #zero
        $ptrbmp = $ptrbmp + 4;
        $cp = unpack("L1", $buf);
        $cpRGBA[$i] = $cp;
        ####printf("1 ColorPallet[%d] = 0x%08x\n", $i, $cp);
    }
    for ($i = 0; $i < ($bodyOffset - 0x36 - $cpUsed * 4); $i++)
    {
        read (IN_bmp, $buf, 1); # skip
        $ptrbmp = $ptrbmp + 1;
    }
}
else
{
    for ($i = 0; $i < ($bodyOffset - 0x36); $i++)
    {
        read (IN_bmp, $buf, 1); # skip
        $ptrbmp = $ptrbmp + 1;
    }
}
####printf("Body ptrbmp = 0x%0x\n", $ptrbmp);

#---------------------------
# Read BMP Body
#---------------------------
#
# 8bit
#
if ($pixelDepth == 8)
{
    for ($y = 0; $y < $height; $y++)
    {
        for ($x = 0; $x < $width; $x++)
        {
            read (IN_bmp, $buf, 1);
            $pixel[$height - 1 - $y][$x] = unpack("C1", $buf);
        }
        if ((($width * 1) % 4) != 0) # padding
        {
            for ($x = 0; $x < (4 - (($width * 1) % 4)); $x++)
            {
                read (IN_bmp, $buf, 1); # skip
            } 
        }
    }
}
#
# 24bit
#
elsif ($pixelDepth == 24)
{
    for ($y = 0; $y < $height; $y++)
    {
        for ($x = 0; $x < $width; $x++)
        {
            read (IN_bmp, $buf, 1);
            $blu = unpack("C1", $buf);
            read (IN_bmp, $buf, 1);
            $grn = unpack("C1", $buf);
            read (IN_bmp, $buf, 1);
            $red = unpack("C1", $buf);
            $pixel[$height - 1 - $y][$x] = ($red << 16) + ($grn << 8) + ($blu << 0);
        }
        if ((($width * 3) % 4) != 0) # padding
        {
            for ($x = 0; $x < (4 - (($width * 3) % 4)); $x++)
            {
                read (IN_bmp, $buf, 1); # skip
            } 
        }
    }
}
#
# 32bit
#
else #elsif ($pixelDepth == 32)
{
    for ($y = 0; $y < $height; $y++)
    {
        for ($x = 0; $x < $width; $x++)
        {
            read (IN_bmp, $buf, 4);
            $pixel[$height - 1 - $y][$x] = unpack("L1", $buf);
        }
    }
}

close (IN_bmp);

#------------------------
# Write RGB Body Data
#------------------------
open (OUT_rgb, "> $fname_rgb");
#
# 8bit
#
if ($pixelDepth == 8)
{
    printf(OUT_rgb "static const uint8_t BMP_RGB[] = \n");
    printf(OUT_rgb "{\n");
    $count = 0;

    for ($y = $y0; $y < $y1; $y++)
    {
        for ($x = $x0; $x < $x1; $x++)
        {
            printf(OUT_rgb "0x%02x", $pixel[$y][$x]);
            if (($y != ($y1 - 1)) || ($x != ($x1 - 1)))
            {
                printf(OUT_rgb ", ");
            }            
            $count++;
            if (($count % 8) == 0)
            {
                printf(OUT_rgb "\n");
            }
            
        }
    }
    printf(OUT_rgb "};\n");
}
#
# 24bit or 32bit
#
else #elsif (($pixelDepth == 24) || ($pixelDepth == 32))
{
    printf(OUT_rgb "static const uint32_t BMP_RGB[] =\n");
    printf(OUT_rgb "{\n");
    $count = 0;

    for ($y = $y0; $y < $y1; $y++)
    {
        for ($x = $x0; $x < $x1; $x++)
        {
            printf(OUT_rgb "0x%08x", $pixel[$y][$x]);
            if (($y != ($y1 - 1)) || ($x != ($x1 - 1)))
            {
                printf(OUT_rgb ", ");
            }
            $count++;
            if (($count % 4) == 0)
            {
                printf(OUT_rgb "\n");
            }
        }
    }
    printf(OUT_rgb "};\n");
}

close (OUT_rgb);

#-----------------------
# Write Palette Data
#-----------------------
if ($cpUsed != 0)
{
     open (OUT_plt, "> $fname_plt");

    printf(OUT_plt "#define MAX_PALETTE %d\n", $cpUsed);
    printf(OUT_plt "static const uint32_t BMP_PLT[] =\n");
    printf(OUT_plt "{\n");
    $count = 0;
    
    for ($i = 0; $i < $cpUsed; $i++)
    {
            printf(OUT_plt "0x%08x", $cpRGBA[$i]);
            if ($i != ($cpUsed - 1))
            {
                printf(OUT_plt ", ");
            }
            $count++;
            if (($count % 4) == 0)
            {
                printf(OUT_plt "\n");
            }
    }
    
    printf(OUT_plt "};\n");
    close (OUT_plt);
}


#--------------------
# End of Main Routine
#--------------------
exit(0);

#====================
# End of the Program.
#====================
