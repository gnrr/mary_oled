//=========================================================
// LPC1114 Project
//=========================================================
// File Name : main.c
// Function  : Main Routine
//---------------------------------------------------------
// Rev.01 2010.08.01 Munetomo Maruyama
//---------------------------------------------------------
// Copyright (C) 2010-2011 Munetomo Maruyama
//=========================================================
// ---- License Information -------------------------------
// Anyone can FREELY use this code fully or partially
// under conditions shown below.
// 1. You may use this code only for individual purpose,
//    and educational purpose.
//    Do not use this code for business even if partially.
// 2. You can copy, modify and distribute this code.
// 3. You should remain this header text in your codes
//   including Copyright credit and License Information.
// 4. Your codes should inherit this license information.
//=========================================================
// ---- Patent Notice -------------------------------------
// I have not cared whether this system (hw + sw) causes
// infringement on the patent, copyright, trademark,
// or trade secret rights of others. You have all
// responsibilities for determining if your designs
// and products infringe on the intellectual property
// rights of others, when you use technical information
// included in this system for your business.
//=========================================================
// ---- Disclaimers ---------------------------------------
// The function and reliability of this system are not
// guaranteed. They may cause any damages to loss of
// properties, data, money, profits, life, or business.
// By adopting this system even partially, you assume
// all responsibility for its use.
//=========================================================

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include "color_led.h"
#include "oled.h"
#include "systick.h"

//=====================
// Display BITMAP
//=====================
//-----------------------
// Main Routine
//-----------------------
#define BMP 1   // select which BMP you want to see.
#define BUD 30  // select which BUD part you want to see.

#if BMP == 0
  #include "../bmp/board8.bmp.plt.h"
  #include "../bmp/board8.bmp.rgb.h"
#endif

#if BMP == 1
  #include "../bmp/lego8.bmp.plt.h"
  #include "../bmp/lego8.bmp.rgb.h"
#endif

#if BMP == 2
  #include "../bmp/nana8.bmp.plt.h"
  #include "../bmp/nana8.bmp.rgb.h"
#endif

#if BMP == 3
  #include "../bmp/bud.plt.h"
  #if BUD == 00
    #include "../bmp/bud00.rgb.h"
  #endif
  #if BUD == 01
    #include "../bmp/bud01.rgb.h"
  #endif
  #if BUD == 02
    #include "../bmp/bud02.rgb.h"
  #endif
  #if BUD == 03
    #in3lude "../bmp/bud03.rgb.h"
  #endif
  #if BUD == 10
    #include "../bmp/bud10.rgb.h"
  #endif
  #if BUD == 11
    #include "../bmp/bud11.rgb.h"
  #endif
  #if BUD == 12
    #include "../bmp/bud12.rgb.h"
  #endif
  #if BUD == 13
    #in3lude "../bmp/bud13.rgb.h"
  #endif
  #if BUD == 20
    #include "../bmp/bud20.rgb.h"
  #endif
  #if BUD == 21
    #include "../bmp/bud21.rgb.h"
  #endif
  #if BUD == 22
    #include "../bmp/bud22.rgb.h"
  #endif
  #if BUD == 23
    #in3lude "../bmp/bud23.rgb.h"
  #endif
  #if BUD == 30
    #include "../bmp/bud30.rgb.h"
  #endif
  #if BUD == 31
    #include "../bmp/bud31.rgb.h"
  #endif
  #if BUD == 32
    #include "../bmp/bud32.rgb.h"
  #endif
  #if BUD == 33
    #include "../bmp/bud33.rgb.h"
  #endif
#endif
//
int main(void)
{
    uint32_t x, y, i;
    uint8_t  index;
    uint32_t palette;
    uint32_t red, blu, grn;
    uint32_t pixel;
    //
    // Initialization
    //
    Init_SysTick();
    // Init_Color_LED();
    Init_OLED();
    //
    // Send Bitmap Data
    //
    i = 0;
    for (y = 0; y < 128; y++)
    {
        for (x = 0; x < 128; x++)
        {
            index = BMP_RGB[i++];
            palette = BMP_PLT[index];

            red = (palette >> (16 + 3)) & 0x01f;
            grn = (palette >> ( 8 + 2)) & 0x03f;
            blu = (palette >> ( 0 + 3)) & 0x01f;
            pixel = (red << 11) + (grn << 5) + (blu << 0);
            OLED_Draw_Dot(x, y, 1, pixel);
        }
    }
    //
    // Finish Sign
    //
    while(1)
    {
        // Draw_Color_LED();
    }
    return 0;
}

//=========================================================
// End of Program
//=========================================================
